## 0.2.1-rc0 (2023-09-11)

### Fix

- removing no-commit-to-branch hook of pre-commit

## 0.2.0 (2023-09-06)

### Feat

- only run pipeline on main branch

## 0.1.1 (2023-09-06)

### Fix

- adjust generate_version step

## 0.1.0 (2023-08-29)

### Feat

- **.gitlab-ci.yaml**: change how pipeline runs

### Fix

- **.gitlab-ci.yaml**: remove pytest step for now
- **.gitlab-ci.yaml**: adjusting pytest use
- **.gitlab-ci.yaml**: correctly use pylint and pytest on pipeline
